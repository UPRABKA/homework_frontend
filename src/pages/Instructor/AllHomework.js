import React from "react";
import { Card,Grid } from "@material-ui/core";
import PeopleIcon from "@material-ui/icons/People";
import { makeStyles } from "@material-ui/core/styles";
import { Dashboard } from "../dashboard/dashboard";
import Homework from "../../component/card-content/all";
import SearchButton from "../../component/button/index";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    pageHeader: {
        backgroundColor: "white",
        height: "auto",
        display: "flex",
        alignItems: "center",
    },
    headerIconBorder: {
        paddingLeft: "0.8vw",
        paddingRight: "0.8vw",
        margin: "1.6vw",
        padding: "0.6vw",
    },
    headerIcon: {
        color: "#3f51b5",
        fontSize: "2.8vw",
    },
    gridItem: {
        display: "flex",
        justifyContent: "center",
    },
    headText: {
        width: "30vw",
        fontFamily: "rubik",
        fontWeight: "bold",
        color: "white",
        backgroundColor: "black",
    },  
    cardRoot: {
        width: "auto",
        padding: "3vw",
        marginBottom: "3vw",

    },
    formLabel: {
        width: "8vw",
        fontWeight: "bold",
        fontSize: "1.3vw",
        color: "black",
        marginLeft: "2vw",
    },
    '@global': {
        '*::-webkit-scrollbar': {
            width: '1em'
        },
    },
    '@global': {
        '*::-webkit-scrollbar': {
            width: '1em'
        },
    },
}));

const  AllHomework  = (props) => {

const classes=useStyles();

return(
    <div className={classes.root}>
        <Dashboard navbarName={"All Homeworks"}/>
        <main className={classes.content}>
                <div className={classes.appBarSpacer}/>
                <Grid container>
                    <Grid item xs={12} md={12} lg={12}>
                        <div className={classes.pageHeader}>
                            <Card className={classes.headerIconBorder}>
                                <PeopleIcon className={classes.headerIcon}/>
                            </Card>
                        </div>
                    </Grid>
                    <Grid item xs={12} md={12} lg={12} className={classes.gridItem}>
                        <SearchButton buttonName="Homework Title:"/>
                        </Grid>
                        <Grid item xs={12} md={12} lg={12} className={classes.gridItem}>
                        <Card>
                            <Homework/>
                        </Card>
                        </Grid>
                </Grid>
        </main>
    </div>
);

};

export default AllHomework;