import React from "react";
import { Dashboard } from "../dashboard/dashboard";
import { makeStyles } from "@material-ui/core/styles";
import { Card, Grid } from "@material-ui/core";
import AccountBalanceOutlinedIcon from "@material-ui/icons/AccountBalanceOutlined";
import Cardcontent from "../../component/card-content/update";
import SearchButton from "../../component/button/index";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
    },
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
        backgroundColor: "#E4E7EB",
    },
    appBarSpacer: theme.mixins.toolbar,
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    imgProfileImg: {
        width: "8vw",
        height: "8vw",
        objectFit: "cover",
        [theme.breakpoints.only('sm')]: {
            width: "15vw",
            height: "15vw",
        },
        [theme.breakpoints.only('xs')]: {
            width: "25vw",
            height: "25vw",
        },
    },
    inputImage: {
        display: "none",
    },
    divProfileImg: {
        backgroundColor: "#E4E7EB",
        width: "8vw",
        height: "8vw",
        marginLeft: "2.5vw",
        marginTop: "1.5vw",
        borderRadius: "15px",
        overflow: "hidden",
        [theme.breakpoints.only('sm')]: {
            width: "15vw",
            height: "15vw",
        },
        [theme.breakpoints.only('xs')]: {
            width: "25vw",
            height: "25vw",
        },
    },
    pageHeader: {
        backgroundColor: "white",
        height: "auto",
        display: "flex",
        alignItems: "center",
    },
    headerIconBorder: {
        paddingLeft: "0.8vw",
        paddingRight: "0.8vw",
        margin: "1.6vw",
        padding: "0.6vw",
    },
    headerIcon: {
        color: "#3f51b5",
        fontSize: "2.8vw",

    },
    title: {
        marginLeft: "0.5vw",
        fontSize: "1.5vw",
        fontFamily: "rubik",
        fontWeight: "bolder",
    },
    gridItem: {
        display: "flex",
        justifyContent: "center",
    },
    cardRoot: {
        marginTop: "3vw",
        width: "60vw",
        padding: "3vw",
        marginBottom: "3vw",

    },
    labelChooseFile: {
        backgroundColor: "#E4E7EB",
        marginLeft: "2.4vw",
        marginTop: "1vw",
        fontFamily: "rubik",
        fontSize: "0.8vw",
        paddingLeft: "2vw",
        paddingRight: "2vw",
        paddingTop: "0.5vw",
        paddingBottom: "0.5vw",
        borderRadius: "15px",
        textTransform: "none",
        cursor: "pointer",
        [theme.breakpoints.only('sm')]: {
            fontSize: "1.4vw",
            borderRadius: "8px",
        },
        [theme.breakpoints.only('xs')]: {
            fontSize: "2.3vw",
            borderRadius: "6px",
            paddingTop: "0.8vw",
            paddingBottom: "0.8vw",
            marginTop: "1vw",

        },
    },
    '@global': {
        '.MuiInput-underline:after': {
            borderBottom: "0px",
        },
        '.MuiInput-underline:before': {
            borderBottom: "0px",
        },
        '.MuiInput-underline:hover:not(.Mui-disabled):before': {
            borderBottom: "0px",
        },
        '.MuiFilledInput-underline:before': {
            borderBottom: "0px",
        },
        '.MuiFilledInput-underline:after': {
            borderBottom: "0px",
        },
        '.MuiFilledInput-underline:hover:before': {
            borderBottom: "0px",
        },
        '.MuiFilledInput-root.Mui-focused': {
            backgroundColor: "#E4E7EB",
        },
        '.MuiFilledInput-root': {
            backgroundColor: "#E4E7EB",
        },
        '*::-webkit-scrollbar': {
            width: '0em'
        },
        '.MuiFilledInput-root:hover': {
            backgroundColor: "#E4E7EB",
        },
       
    }
}))



const UpdateHomework =(props) => {
    const classes = useStyles();
    return(
        <div className={classes.root}>
            <Dashboard navbarName={"Update Homework"}/>
            <main className={classes.content}>
            <div className={classes.appBarSpacer}/>
                <Grid container>
                        <Grid item xs={12} md={12} lg={12}>
                            <div className={classes.pageHeader}>
                                <Card className={classes.headerIconBorder}>
                                    <AccountBalanceOutlinedIcon className={classes.headerIcon}/>
                                </Card>
                            </div>
                        </Grid>
                        <Grid item xs={12} md={12} lg={12} className={classes.gridItem}>
                            <SearchButton buttonName="Homework Title:"/>
                                {/*<div className={classes.rowDetail1}
                                 style={{
                                    borderRadius: "15px",
                                     height: "4vw",
                                     backgroundColor: "white",
                                 }}
                                 >
                                <div style={{
                                    width: "20vw",
                                    marginLeft: "5vw",
                                    fontWeight: "bold",
                                    fontSize: "1.3vw",
                                }}>
                                    Homework Title:
                                </div>
                                <div className={classes.inputStyleDiv1} style={{width:'15vw'}}>
                                        <Input  style={{width:'15vw',height:"3vw", backgroundColor:"#E4E7EB", borderRadius:"2vw"}}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="text"/>
                                </div>
                                <div>
                                    <Button className={classes.btnUpdate} style={{marginRight:"2vw"}}>Go</Button>
                                </div>
                                </div>*/}
                        </Grid>
                        <Grid item xs={12} md={12} lg={12} className={classes.gridItem}>
                        <Card className={classes.cardRoot}>
                            <Cardcontent/>          
                        </Card>
                        </Grid>
                </Grid>
            </main>
            </div>
);

};

export default UpdateHomework;