import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Card, Grid} from "@material-ui/core";
import AccountBalanceOutlinedIcon from "@material-ui/icons/AccountBalanceOutlined";
import {TableContainer} from "@material-ui/core";
import { Dashboard } from "./dashboard/dashboard";
import Users from "../component/tables/user";
import SearchButton from "../component/button/index";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
    },
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
        backgroundColor: "#E4E7EB",
    },
    appBarSpacer: theme.mixins.toolbar,
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    imgProfileImg: {
        width: "8vw",
        height: "8vw",
        objectFit: "cover",
        [theme.breakpoints.only('sm')]: {
            width: "15vw",
            height: "15vw",
        },
        [theme.breakpoints.only('xs')]: {
            width: "25vw",
            height: "25vw",
        },
    },
    inputImage: {
        display: "none",
    },
    divProfileImg: {
        backgroundColor: "#E4E7EB",
        width: "8vw",
        height: "8vw",
        marginLeft: "2.5vw",
        marginTop: "1.5vw",
        borderRadius: "15px",
        overflow: "hidden",
        [theme.breakpoints.only('sm')]: {
            width: "15vw",
            height: "15vw",
        },
        [theme.breakpoints.only('xs')]: {
            width: "25vw",
            height: "25vw",
        },
    },
    pageHeader: {
        backgroundColor: "white",
        height: "auto",
        display: "flex",
        alignItems: "center",
    },
    headerIconBorder: {
        paddingLeft: "0.8vw",
        paddingRight: "0.8vw",
        margin: "1.6vw",
        padding: "0.6vw",
    },
    headerIcon: {
        color: "#3f51b5",
        fontSize: "2.8vw",

    },
    title: {
        marginLeft: "0.5vw",
        fontSize: "1.5vw",
        fontFamily: "rubik",
        fontWeight: "bolder",
    },
    gridItem: {
        display: "flex",
        justifyContent: "center",
    },
    cardRoot: {
        marginTop: "3vw",
        width: "80vw",
        padding: "1vw",
        marginBottom: "3vw",

    },
    row: {
        display: "flex",
        flexFlow: "column",

    },
    rowDetails: {
        display: "flex", flexFlow: "row", justifyContent: "space-around"
    },
    rowInput: {},
    rowDetail: {
        width: "6.5vw",
        height: "4vw",
        fontFamily: "rubik",
        fontWeight: "bold",
        display: "flex",
        alignItems: "center",
        fontSize: "1.1vw",
        [theme.breakpoints.down('sm')]: {
            fontSize: "1.8vw",
            height: "5vw",
            width: "16vw",
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: "3vw",
            height: "9vw",
            width: "26vw",
        },
    },
    rowDetail: {
        height: "4vw",
        fontFamily: "rubik",
        fontWeight: "bold",
        display: "flex",
        alignItems: "center",
        fontSize: "1.1vw",
        [theme.breakpoints.down('sm')]: {
            fontSize: "1.8vw",
            height: "5vw",
            width: "16vw",
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: "3vw",
            height: "9vw",
            width: "26vw",
        },
    },
    inputStyleDiv: {
        height: "4vw",
        width: "13.5vw",
        display: "flex",
        marginRight:"2vw",
        backgroundColor:"white",
        //alignItems: "center",
        [theme.breakpoints.down('sm')]: {
            height: "5vw",
            width: "25vw",
        },
        [theme.breakpoints.down('xs')]: {
            height: "9vw",
            width: "35vw",
        }

    },
    inputStyle: {
        width: "10vw",
        backgroundColor: "#E4E7EB",
        borderRadius: "5px",
        paddingLeft: "10px",
        paddingRight: "10px",
        fontFamily: "rubik",
        fontSize: "1.2vw",
        margin: "0.5vw",
        height:"3vw",
        [theme.breakpoints.down('sm')]: {
            fontSize: "1.8vw",
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: "3vw",
        },
    },
    btnSubmitDiv: {
        marginTop:"3vw",
        marginRight: "5vw",
        display: "flex",
        justifyContent: "flex-end",
    },
    btnSubmit:{
        fontSize:"1vw",
        fontWeight:"bold",
        fontFamily:"rubik",
        backgroundColor:"#3f51b5",
        color:"white",
        borderRadius:"10px",
        minWidth:"5vw",
        '&:hover':{
            backgroundColor:"E4E7EB",
            color:"#3f51b5"
        }
    },
    labelChooseFile: {
        backgroundColor: "#E4E7EB",
        marginLeft: "2.4vw",
        marginTop: "1vw",
        fontFamily: "rubik",
        fontSize: "0.8vw",
        paddingLeft: "2vw",
        paddingRight: "2vw",
        paddingTop: "0.5vw",
        paddingBottom: "0.5vw",
        borderRadius: "15px",
        textTransform: "none",
        cursor: "pointer",
        [theme.breakpoints.only('sm')]: {
            fontSize: "1.4vw",
            borderRadius: "8px",
        },
        [theme.breakpoints.only('xs')]: {
            fontSize: "2.3vw",
            borderRadius: "6px",
            paddingTop: "0.8vw",
            paddingBottom: "0.8vw",
            marginTop: "1vw",

        },
    },
 
    formLabel: {
        width: "8vw",
        fontWeight: "bold",
        fontSize: "1.3vw",
        color: "black",
        marginLeft: "2vw",
    },
    '@global': {
        '*::-webkit-scrollbar': {
            width: '1em'
        },
    },
   tableContainer: {
    margin: "2vw",
    width: "auto",
    height: "48vw",
    overflowY: "scroll",
},
    '@global': {
        '.MuiInput-underline:after': {
            borderBottom: "0px",
        },
        '.MuiInput-underline:before': {
            borderBottom: "0px",
        },
        '.MuiInput-underline:hover:not(.Mui-disabled):before': {
            borderBottom: "0px",
        },
        '.MuiFilledInput-underline:before': {
            borderBottom: "0px",
        },
        '.MuiFilledInput-underline:after': {
            borderBottom: "0px",
        },
        '.MuiFilledInput-underline:hover:before': {
            borderBottom: "0px",
        },
        '.MuiFilledInput-root.Mui-focused': {
            backgroundColor: "#E4E7EB",
        },
        '.MuiFilledInput-root': {
            backgroundColor: "#E4E7EB",
        },
        '*::-webkit-scrollbar': {
            width: '0em'
        },
        '.MuiFilledInput-root:hover': {
            backgroundColor: "#E4E7EB",
        },
       
    }
}))

const AllUsers =() => {
const classes = useStyles();

return(
    <div className={classes.root}>
        <Dashboard navbarName={"View Homeworks"}/>
        <main className={classes.content}>
                <div className={classes.appBarSpacer}/>
                <Grid container>
                    <Grid item xs={12} md={12} lg={12}>
                        <div className={classes.pageHeader}>
                            <Card className={classes.headerIconBorder}>
                                <AccountBalanceOutlinedIcon className={classes.headerIcon}/>
                            </Card>
                        </div>
                    </Grid>
                    <Grid item xs={12} md={12} lg={12} className={classes.gridItem}>
                            <SearchButton buttonName="User Name:"/>
                    </Grid>
                    <Grid item xs={12} md={12} lg={12} className={classes.gridItem}>
                    <TableContainer className={classes.tableContainer}>
                        <Users/>
                    </TableContainer>
                    </Grid>
                </Grid>

            </main>
    </div>
);

};

export default AllUsers;