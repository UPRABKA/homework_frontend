import React, { useState } from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Button, Card, CardContent, Grid, Input} from "@material-ui/core";
import AccountBalanceOutlinedIcon from "@material-ui/icons/AccountBalanceOutlined";
import {postUserData } from "../api/authenticationService";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
    },
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
        backgroundColor: "#E4E7EB",
    },
    appBarSpacer: theme.mixins.toolbar,
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    imgProfileImg: {
        width: "8vw",
        height: "8vw",
        objectFit: "cover",
        [theme.breakpoints.only('sm')]: {
            width: "15vw",
            height: "15vw",
        },
        [theme.breakpoints.only('xs')]: {
            width: "25vw",
            height: "25vw",
        },
    },
    inputImage: {
        display: "none",
    },
    divProfileImg: {
        backgroundColor: "#E4E7EB",
        width: "8vw",
        height: "8vw",
        marginLeft: "2.5vw",
        marginTop: "1.5vw",
        borderRadius: "15px",
        overflow: "hidden",
        [theme.breakpoints.only('sm')]: {
            width: "15vw",
            height: "15vw",
        },
        [theme.breakpoints.only('xs')]: {
            width: "25vw",
            height: "25vw",
        },
    },
    pageHeader: {
        backgroundColor: "white",
        height: "auto",
        display: "flex",
        alignItems: "center",
    },
    headerIconBorder: {
        paddingLeft: "0.8vw",
        paddingRight: "0.8vw",
        margin: "1.6vw",
        padding: "0.6vw",
    },
    headerIcon: {
        color: "#3f51b5",
        fontSize: "2.8vw",

    },
    title: {
        marginLeft: "0.5vw",
        fontSize: "1.5vw",
        fontFamily: "rubik",
        fontWeight: "bolder",
    },
    gridItem: {
        display: "flex",
        justifyContent: "center",
    },
    cardRoot: {
        marginTop: "3vw",
        width: "60vw",
        padding: "3vw",
        marginBottom: "3vw",

    },
    row: {
        display: "flex",
        flexFlow: "column",

    },
    rowDetails: {
        display: "flex", flexFlow: "row", justifyContent: "space-around"
    },
    rowInput: {},
    rowDetail: {
        width: "6.5vw",
        height: "4vw",
        fontFamily: "rubik",
        fontWeight: "bold",
        display: "flex",
        alignItems: "center",
        fontSize: "1.1vw",
        [theme.breakpoints.down('sm')]: {
            fontSize: "1.8vw",
            height: "5vw",
            width: "16vw",
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: "3vw",
            height: "9vw",
            width: "26vw",
        },
    },
    inputStyleDiv: {
        height: "4vw",
        width: "13.5vw",
        display: "flex",
        alignItems: "center",
        [theme.breakpoints.down('sm')]: {
            height: "5vw",
            width: "25vw",
        },
        [theme.breakpoints.down('xs')]: {
            height: "9vw",
            width: "35vw",
        }

    },
    inputStyle: {
        width: "13.5vw",
        backgroundColor: "#E4E7EB",
        borderRadius: "25px",
        paddingLeft: "15px",
        paddingRight: "10px",
        fontFamily: "rubik",
        fontSize: "1.2vw",
        marginLeft: "0.5vw",
        [theme.breakpoints.down('sm')]: {
            fontSize: "1.8vw",
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: "3vw",
        },
    },
    btnSubmitDiv: {
        marginTop:"3vw",
        marginRight: "5vw",
        display: "flex",
        justifyContent: "flex-end",
    },
    btnSubmit:{
        fontSize:"1vw",
        fontWeight:"bold",
        fontFamily:"rubik",
        backgroundColor:"#3f51b5",
        color:"white",
        borderRadius:"10px",
        marginLeft:"2vw",
        minWidth:"5vw",
        '&:hover':{
            backgroundColor:"E4E7EB",
            color:"#3f51b5"
        }
    },
    labelChooseFile: {
        backgroundColor: "#E4E7EB",
        marginLeft: "2.4vw",
        marginTop: "1vw",
        fontFamily: "rubik",
        fontSize: "0.8vw",
        paddingLeft: "2vw",
        paddingRight: "2vw",
        paddingTop: "0.5vw",
        paddingBottom: "0.5vw",
        borderRadius: "15px",
        textTransform: "none",
        cursor: "pointer",
        [theme.breakpoints.only('sm')]: {
            fontSize: "1.4vw",
            borderRadius: "8px",
        },
        [theme.breakpoints.only('xs')]: {
            fontSize: "2.3vw",
            borderRadius: "6px",
            paddingTop: "0.8vw",
            paddingBottom: "0.8vw",
            marginTop: "1vw",

        },
    },
    '@global': {
        '.MuiInput-underline:after': {
            borderBottom: "0px",
        },
        '.MuiInput-underline:before': {
            borderBottom: "0px",
        },
        '.MuiInput-underline:hover:not(.Mui-disabled):before': {
            borderBottom: "0px",
        },
        '.MuiFilledInput-underline:before': {
            borderBottom: "0px",
        },
        '.MuiFilledInput-underline:after': {
            borderBottom: "0px",
        },
        '.MuiFilledInput-underline:hover:before': {
            borderBottom: "0px",
        },
        '.MuiFilledInput-root.Mui-focused': {
            backgroundColor: "#E4E7EB",
        },
        '.MuiFilledInput-root': {
            backgroundColor: "#E4E7EB",
        },
        '*::-webkit-scrollbar': {
            width: '0em'
        },
        '.MuiFilledInput-root:hover': {
            backgroundColor: "#E4E7EB",
        },
       
    }
}))


const RegisterUser =() => {
const classes = useStyles();

const [values, setValues] = useState({
    id: '',
    name: '',
    username:'',
    password:'',
    role:''
    });

const handleSubmit=(evt)=>{
    evt.preventDefault();     
    
    postUserData(values).then(()=>{
            console.info("Add new user")
         });
     }

return(
    <div className={classes.root}>
        <main className={classes.content}>
                <div className={classes.appBarSpacer}/>
                <Grid container>
                    <Grid item xs={12} md={12} lg={12}>
                        <div className={classes.pageHeader}>
                            <Card className={classes.headerIconBorder}>
                                <AccountBalanceOutlinedIcon className={classes.headerIcon}/>
                            </Card>
                        </div>
                    </Grid>
                    <Grid item xs={12} md={12} lg={12} className={classes.gridItem}>
                        <Card className={classes.cardRoot}>
                            <CardContent>
                                <form onSubmit={handleSubmit} noValidate={false}>
                                    <div className={classes.row}>
                                        <div className={classes.rowDetails}
                                             style={{justifyContent: "space-between", marginLeft: "2vw",marginRight:"6vw", marginBottom:"2vw"}}>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail} style={{width: "8vw"}}>User Id :
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           style={{width:'13vw'}}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="text"
                                                           value={values.id}
                                                           onChange={(e) => {
                                                            setValues({...values,id:e.target.value})
                                                        }} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className={classes.rowDetails}>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail}
                                                     style={{width: "10vw",justifyContent: "space-between", marginLeft: "2vw",marginRight:"0vw"}}>User Name:
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="text"
                                                           value={values.name}
                                                           onChange={(e) => {
                                                            setValues({...values,name:e.target.value})
                                                        }} />
                                                </div>
                                            </div>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail}
                                                     style={{width: "8vw",justifyContent: "space-between", marginLeft: "3vw",marginRight:"0vw"}}>Username:
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="textarea"
                                                           value={values.username}
                                                           onChange={(e) => {
                                                            setValues({...values,username:e.target.value})
                                                        }}  />
                                                </div>
                                            </div>
                                        </div>
                                        <div className={classes.rowDetails}>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail}
                                                     style={{width: "10vw",justifyContent: "space-between", marginLeft: "2vw",marginRight:"0vw"}}>Password:
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="text"
                                                           value={values.password}
                                                           onChange={(e) => {
                                                            setValues({...values,password:e.target.value})
                                                        }} />
                                                </div>
                                            </div>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail}
                                                     style={{width: "8vw",justifyContent: "space-between", marginLeft: "3vw",marginRight:"0vw"}}>Role:
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="text"
                                                           value={values.role}
                                                           onChange={(e) => {
                                                            setValues({...values,role:e.target.value})
                                                        }}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={classes.btnSubmitDiv}>
                                        <Button type={"submit"} className={classes.btnSubmit}>Submit</Button>
                                        <Button type={"reset"} className={classes.btnSubmit}>Clear</Button>
                                    </div>
                                </form>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>

            </main>
    </div>
);

};

export default RegisterUser;