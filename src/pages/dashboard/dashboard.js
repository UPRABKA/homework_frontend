import React,{useState} from "react";
import { useDispatch } from "react-redux";
import { fetchUserData } from "../../api/authenticationService";
import { AppBar, Toolbar,IconButton,CssBaseline, Menu, Typography, makeStyles, Avatar, MenuItem ,Divider } from "@material-ui/core";
import { List } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import clsx from "clsx";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import Drawer from "@material-ui/core/Drawer";
import Listitem from '../../component/appbar-list/index';

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({

    root:{
        display:'flex',
    },
    fixedHeight:{
        height:240,
    },

    nested:{
        paddingLeft:theme.spacing(4)
    },

    toolbar:{
        paddingRight:24,
    },
    toolbarIcon:{
        display:'flex',
        alignItems:'center',
        justifyContent:'flex-end',
        padding:'0 8px',
        ...theme.mixins.toolbar,
    },


    title:{
        flexGrow:1,
        fontFamily:"rubik",
        fontWeight:"bold"
    },
    paper:{
        padding:theme.spacing(2),
        display:'flex',
        overflow:'auto',
        flexDirection:'column',
    },
    drawerPaper:{
        position:"relative",
        whiteSpace:'nowrap',
        width:drawerWidth,
        transition:theme.transitions.create('width',{
            easing:theme.transitions.easing.sharp,
            durations:theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose:{
        overflowX:'hidden',
        transition:theme.transitions.create('width',{
            easing:theme.transitions.easing.sharp,
            durations:theme.transitions.duration.enteringScreen,
        }),
        width:theme.spacing(7),
        [theme.breakpoints.up('sm')]:{
            width:theme.spacing(9),
        },
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
}));


export const Dashboard=(props)=>{

    const [open,setOpen]=React.useState(true);
    const [anchor,setAnchor]=React.useState(null);
    const isMenuOpen=Boolean(anchor);

    const handeleDrawerOpen = () =>{
        setOpen(true);
    }
    const handeleDrawerClose = () =>{
        setOpen(false);
    }

    const handelemenuOpen = () => {
        setAnchor(null);
    }

    const handelMenuClose =(event) => {
        setAnchor(event.currentTarget);
    }

    const menuId='primary-search-account-menu';
    const rendermenu=(
        <Menu 
            anchorEl={anchor}
            id={menuId}
            keepMounted
            open={isMenuOpen}
            onClose={handelMenuClose}
            PaperProps={{
                style:{
                    marginTop:"40px",
                    backgroundColor:"#E4e7EB",
                    color:"black",
                    fontWeight:"bold"
                }
            }}
            >

            <MenuItem>Log Out</MenuItem>
        </Menu>
    );

    const classes=useStyles();

    const dispatch=useDispatch();
    const [loading,setLoading]=useState(false);
    const [data,setData]=useState({});

    React.useEffect(()=>{
        fetchUserData().then((response)=>{
            setData(response.data);
        }).catch((e)=>{
            localStorage.clear();
            props.history.push('/');
        })
    },[])

    return (
    <div>
       <CssBaseline/>
           <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)} >
               <Toolbar className={classes.toolbar}>
                   <IconButton 
                        edge="start" 
                        color="inherit" 
                        aria-label="open drawer" 
                        onClick={handeleDrawerOpen}
                        className={clsx(classes.menuButton, open && classes.menuButtonHidden)}>
                        <MenuIcon/>
                   </IconButton>
                   <Typography component="h1" varient="h6" color="inherit" noWrap className={classes.title}>
                        {props.navbarName}
                   </Typography>
                   <IconButton 
                   aria-label="homework manage"
                   aria-controls={menuId}
                   aria-haspopup="true"
                   color="inherit"
                   onClick={handelemenuOpen}>
                       <h4>Hello <span style={{marginRight:"1vw"}}> {data && `${data.username}`} </span></h4>
                       <Avatar alt="logged person"></Avatar>
                   </IconButton>
                   {rendermenu}
               </Toolbar>
               </AppBar>
               <Drawer
                    varient="permanent"
                    classes={{
                        paper:clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
                    }}
                    open={open}>
                    <div>
                        <IconButton onClick={handeleDrawerClose}>
                            <ChevronLeftIcon/>
                        </IconButton>
                    </div>
                    <Divider/>
                    <List>
                        <Listitem/>
                    </List>

               </Drawer>
    </div>
    );
};