import axios from "axios";


const getToken=()=>{
    return localStorage.getItem("USER_KEY");
};

export const userLogin=(authRequest)=>{
    return axios({
        "method":"POST",
        "url":`${process.env.hostUrl||'http://localhost:8080'}/api/auth`,
        "data":authRequest
    });
}

export const fetchUserData=(authRequest)=>{
    return axios({
        method:"GET",
        url:`${process.env.hostUrl||'http://localhost:8080'}/api/user`,
        headers:{
            "Authorization":"Bearer "+getToken(),
            "Access-Control-Allow-Origin":"*",
            "Access-Control-Allow-Credentials" : true
        }
    });
}

export const fetchHomeworkData=(authRequest)=>{
    return axios({
        method:"GET",
        url:`${process.env.hostUrl||'http://localhost:8080'}/api/homework/all`,
        headers:{
            "Authorization":"Bearer "+getToken(),
            "Access-Control-Allow-Origin":"*",
            "Access-Control-Allow-Credentials" : true
        }
    });
}

export const fetchUsersData=(authRequest)=>{
    return axios({
        method:"GET",
        url:`${process.env.hostUrl||'http://localhost:8080'}/api/user/all`,
        headers:{
            "Authorization":"Bearer "+getToken(),
            "Access-Control-Allow-Origin":"*",
            "Access-Control-Allow-Credentials" : true
        }
    });
}

export const postHomeworkData=(request)=>{
    return axios({
        method:"POST",
        url:`${process.env.hostUrl||'http://localhost:8080'}/api/homework/`,
        headers:{
            "Authorization":"Bearer "+getToken(),
            "Access-Control-Allow-Origin":"*",
            "Access-Control-Allow-Credentials" : true,
            "Content-Type": "application/json",
        },
        data:request,
        body: JSON.stringify(request)
    });
}

export const postUserData=(request)=>{
    return axios({
        method:"POST",
        url:`${process.env.hostUrl||'http://localhost:8080'}/api/user/`,
        headers:{
            //"Authorization":"Bearer "+getToken(),
            "Access-Control-Allow-Origin":"*",
            "Access-Control-Allow-Credentials" : true,
            "Content-Type": "application/json",
        },
        data:request,
        body: JSON.stringify(request)
    });
}

export const updateHomeworkData=(request)=>{
    return axios({
        method:"PUT",
        url:`${process.env.hostUrl||'http://localhost:8080'}/api/homework/`,
        headers:{
            "Authorization":"Bearer "+getToken(),
            "Access-Control-Allow-Origin":"*",
            "Access-Control-Allow-Credentials" : true,
            "Content-Type": "application/json",
        },
        data:request,
        body: JSON.stringify(request)
    });
}

export const updateUserkData=(request)=>{
    return axios({
        method:"PUT",
        url:`${process.env.hostUrl||'http://localhost:8080'}/api/homework/`,
        headers:{
            "Authorization":"Bearer "+getToken(),
            "Access-Control-Allow-Origin":"*",
            "Access-Control-Allow-Credentials" : true,
            "Content-Type": "application/json",
        },
        data:request,
        body: JSON.stringify(request)
    });
}
//post and put id for homework
export const deleteUserData=(id)=>{
    return axios({
        method:"DELETE",
        url:`${process.env.hostUrl||'http://localhost:8080'}/api/homework/$${id}`,
        headers:{
            "Authorization":"Bearer "+getToken(),
            "Access-Control-Allow-Origin":"*",
            "Access-Control-Allow-Credentials" : true
        }
    });
}
export const deleteHomeworkData=(homeWorkId)=>{
    return axios({
        method:"DELETE",
        url:`${process.env.hostUrl||'http://localhost:8080'}/api/homework/${homeWorkId}`,
        headers:{
            "Authorization":"Bearer "+getToken(),
            "Access-Control-Allow-Origin":"*",
            "Access-Control-Allow-Credentials" : true
        }
    });
}
