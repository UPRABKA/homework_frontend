import "./App.css";
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";
import  LoginPage from "./pages/LoginPage";
import { Dashboard } from "./pages/dashboard/dashboard";
import ViewHomework from "./pages/Student/ViewHomework";
import AllHomework from "./pages/Instructor/AllHomework";
import AddHomework from "./pages/Instructor/AddHomework";
import UpdateHomework from "./pages/Instructor/UpdateHomework";
import AllUsers from "./pages/AllUsers";
import RegisterUser from "./pages/RegisterUser";


function App() {
  return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={LoginPage}/>
          <Route exact path="/dashboard" component={Dashboard}/>
          <Route path="/viewhomeworks" component={ViewHomework}/>
          <Route path="/allhomeworks" component={AllHomework}/>
          <Route path="/addhomework" component={AddHomework}/>
          <Route path="/updatehomework" component={UpdateHomework}/>
          <Route path="/users" component={AllUsers}/>
          <Route path="/register" component={RegisterUser}/>
          </Switch>
      </BrowserRouter>
  );
}

export default App;
