import React from "react";
import { Button, Input} from "@material-ui/core";
import { Typography, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
inputStyleDiv1:{
    height:"4vw",
    width:"25vw",
    display:"flex",
    alignItems:"center",
    backgroundColor:"white",
    marginRight:"2vw",
    borderRadius:"1vw"
},
rowDetail1: {
    height: "4vw",
    fontFamily: "rubik",
    fontWeight: "bold",
    display: "flex",
    alignItems: "center",
    fontSize: "1.1vw",
    marginTop:"2vw",
    [theme.breakpoints.down('sm')]: {
        fontSize: "1.8vw",
        height: "5vw",
        width: "16vw",
    },
    [theme.breakpoints.down('xs')]: {
        fontSize: "3vw",
        height: "9vw",
        width: "26vw",
    },
},
btnUpdate:{
    fontSize:"1vw",
    fontWeight:"bold",
    fontFamily:"rubik",
    backgroundColor:"#3f51b5",
    color:"white",
    borderRadius:"10px",
    minWidth:"5vw",
    '&:hover':{
        backgroundColor:"E4E7EB",
        color:"#3f51b5"
    }
},
}))


const SearchButton =(props) => {
const classes = useStyles();
return(
        <div className={classes.rowDetail1}
                                 style={{
                                    borderRadius: "15px",
                                     height: "4vw",
                                     backgroundColor: "white",
                                 }}>
                                <div style={{
                                    width: "20vw",
                                    marginLeft: "5vw",
                                    fontWeight: "bold",
                                    fontSize: "1.3vw",
                                }}>
                                   <Typography component="h1" varient="h6" color="inherit" noWrap>
                                        {props.buttonName}
                                    </Typography>
                                </div>
                                <div className={classes.inputStyleDiv1} style={{width:'15vw'}}>
                                        <Input  style={{width:'15vw',height:"3vw", backgroundColor:"#E4E7EB", borderRadius:"2vw"}}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="text"/>
                                </div>
                                <div>
                                    <Button className={classes.btnUpdate} style={{marginRight:"2vw"}}>Go</Button>
                                </div>
        </div>
);
};
export default SearchButton;