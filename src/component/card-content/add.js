import React, { useState } from "react";
import { postHomeworkData } from "../../api/authenticationService";
import { makeStyles } from "@material-ui/core/styles";
import { CardContent,Input,Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
row: {
    display: "flex",
    flexFlow: "column",
},
rowDetails: {
    display: "flex", flexFlow: "row", justifyContent: "space-around"
},
rowInput: {},
rowDetail: {
    width: "6.5vw",
    height: "4vw",
    fontFamily: "rubik",
    fontWeight: "bold",
    display: "flex",
    alignItems: "center",
    fontSize: "1.1vw",
    [theme.breakpoints.down('sm')]: {
        fontSize: "1.8vw",
        height: "5vw",
        width: "16vw",
    },
    [theme.breakpoints.down('xs')]: {
        fontSize: "3vw",
        height: "9vw",
        width: "26vw",
    },
},
inputStyleDiv: {
    height: "4vw",
    width: "13.5vw",
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.down('sm')]: {
        height: "5vw",
        width: "25vw",
    },
    [theme.breakpoints.down('xs')]: {
        height: "9vw",
        width: "35vw",
    }
},
inputStyle: {
    width: "13.5vw",
    backgroundColor: "#E4E7EB",
    borderRadius: "25px",
    paddingLeft: "15px",
    paddingRight: "10px",
    fontFamily: "rubik",
    fontSize: "1.2vw",
    marginLeft: "0.5vw",
    [theme.breakpoints.down('sm')]: {
        fontSize: "1.8vw",
    },
    [theme.breakpoints.down('xs')]: {
        fontSize: "3vw",
    },
},
btnSubmitDiv: {
    marginTop:"3vw",
    marginRight: "5vw",
    display: "flex",
    justifyContent: "flex-end",
},
btnSubmit:{
    fontSize:"1vw",
    fontWeight:"bold",
    fontFamily:"rubik",
    backgroundColor:"#3f51b5",
    color:"white",
    borderRadius:"10px",
    marginLeft:"2vw",
    minWidth:"5vw",
    '&:hover':{
        backgroundColor:"E4E7EB",
        color:"#3f51b5"
    }
},
}))

const Cardcontent =() => {

    const classes = useStyles();

    const [values, setValues] = useState({
        homeWorkId: '',
        homeWorkTitle: '',
        description:'',
        courseName:'',
        status:'',
        instructorName:'',
        dueDate:'',
        assignedDate:''
        });

    const handleSubmit=(evt)=>{
        evt.preventDefault();     
        
        postHomeworkData(values).then(()=>{
                console.info("Add new homework");
             });
         }

        return(
                <div>
                            <CardContent>
                                <form noValidate  onSubmit={handleSubmit} noValidate={false}>
                                    <div className={classes.row}>
                                        <div className={classes.rowDetails}
                                             style={{justifyContent: "space-between", marginLeft: "2vw",marginRight:"6vw", marginBottom:"2vw"}}>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail} style={{width: "8vw"}}>Homework Id :
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           style={{width:'13vw'}}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="text"
                                                           value={values.homeWorkId}
                                                           onChange={(e) => {
                                                            setValues({...values,homeWorkId:e.target.value})
                                                        }}  />
                                                </div>
                                            </div>
                                        </div>
                                        <div className={classes.rowDetails}>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail}
                                                     style={{width: "10vw",justifyContent: "space-between", marginLeft: "2vw",marginRight:"0vw"}}>Homework Title:
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="text"
                                                           value={values.homeWorkTitle}
                                                           onChange={(e) => {
                                                            setValues({...values,homeWorkTitle:e.target.value})
                                                        }}  />
                                                </div>
                                            </div>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail}
                                                     style={{width: "8vw",justifyContent: "space-between", marginLeft: "3vw",marginRight:"0vw"}}>Description:
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="textarea"
                                                           value={values.description}
                                                           onChange={(e) => {
                                                            setValues({...values,description:e.target.value})
                                                        }} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className={classes.rowDetails}>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail}
                                                     style={{width: "10vw",justifyContent: "space-between", marginLeft: "2vw",marginRight:"0vw"}}>Instructor name:
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="text"
                                                           value={values.instructorName}
                                                           onChange={(e) => {
                                                            setValues({...values,instructorName:e.target.value})
                                                        }} />
                                                </div>
                                            </div>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail}
                                                     style={{width: "8vw",justifyContent: "space-between", marginLeft: "3vw",marginRight:"0vw"}}>Course Name:
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="text"
                                                           value={values.courseName}
                                                           onChange={(e) => {
                                                            setValues({...values,courseName:e.target.value})
                                                        }}/>
                                                </div>
                                            </div>
                                        </div>
                                           <div className={classes.rowDetails}>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail}
                                                     style={{width: "10vw",justifyContent: "space-between", marginLeft: "2vw",marginRight:"0vw"}}>Status:
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="text"
                                                           value={values.status}
                                                           onChange={(e) => {
                                                            setValues({...values,status:e.target.value})
                                                        }} />
                                                </div>
                                            </div>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail}
                                                     style={{width: "8vw",justifyContent: "space-between", marginLeft: "3vw",marginRight:"0vw"}}>Student Name:
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="text"
                                                           value={values.studentName}
                                                           onChange={(e) => {
                                                            setValues({...values,studentName:e.target.value})
                                                        }}/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className={classes.rowDetails}>                               
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail}
                                                     style={{width: "8vw",justifyContent: "space-between", marginLeft: "3vw",marginRight:"0vw"}}>Assigned Date:
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="date"
                                                           value={values.assignedDate}
                                                           onChange={(e) => {
                                                            setValues({...values,assignedDate:e.target.value})
                                                        }}/>
                                                </div>
                                            </div>
                                            <div style={{display: "flex", flexFlow: "row"}}>
                                                <div className={classes.rowDetail}
                                                     style={{width: "10vw",justifyContent: "space-between", marginLeft: "2vw",marginRight:"0vw"}}>Due Date:
                                                </div>
                                                <div className={classes.inputStyleDiv}>
                                                    <Input className={classes.inputStyle}
                                                           inputProps={{'aria-label': 'description'}}
                                                           disableUnderline
                                                           type="date"
                                                           value={values.dueDate}
                                                           onChange={(e) => {
                                                            setValues({...values,dueDate:e.target.value})
                                                        }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={classes.btnSubmitDiv}>
                                        <Button type={"submit"} className={classes.btnSubmit}>Submit</Button>
                                        <Button type={"reset"} className={classes.btnSubmit}>Clear</Button>
                                    </div>
                                </form>
                            </CardContent>
                 </div>
        );

};

export default Cardcontent;