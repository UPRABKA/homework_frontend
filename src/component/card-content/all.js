import React, { useState } from "react";
import { CardContent, Grid,Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { fetchHomeworkData } from "../../api/authenticationService";
import { deleteHomeworkData } from "../../api/authenticationService";
import { CardText ,Container,CardTitle } from "reactstrap";
import { Link } from "react-router-dom";


const useStyles = makeStyles((theme) => ({
    btnDelete:{
        fontSize:"1vw",
        fontWeight:"bold",
        fontFamily:"rubik",
        backgroundColor:"#f22836",
        color:"white",
        borderRadius:"10px",
        minWidth:"5vw",
        '&:hover':{
            backgroundColor:"E4E7EB",
            color:"#3f51b5"
        }
    },
    btnUpdate:{
        fontSize:"1vw",
        fontWeight:"bold",
        fontFamily:"rubik",
        backgroundColor:"#33cb7f",
        marginRight:"1vw",
        color:"white",
        borderRadius:"10px",
        minWidth:"5vw",
        '&:hover':{
            backgroundColor:"E4E7EB",
            color:"#3f51b5"
        }
    },
}));

const  Homework  = (props) => {

const classes=useStyles();

const [data, setData]=useState([])
  
const [values, setValues] = useState({
    homeWorkId: ''
    });

React.useEffect(()=>{
    fetchHomeworkData().then((response)=>{
        setData(response.data);
    }).catch((e)=>{
        localStorage.clear();
        props.history.push('/');
    })
},[])


return(
    <div>
                <CardContent>
                              
                            {
                                data.map(homework =>
                                    <div style={{width:"800px" }}> 
                                    <Grid container spacing={5} >
                         
                                    <Grid item xs={8} style={{backgroundColor: "#E4E7EB", marginBottom:"3vw" }}>
                                       
                                        <CardTitle>HomeWork Title : {homework.homeWorkTitle}</CardTitle>
                                        <CardText>{homework.description}</CardText>
                                        <CardText>{homework.instructorName}</CardText>
                                        <CardText>{homework.courseName}</CardText>
                                        <CardText>{homework.status}</CardText>
                                        <CardText>{homework.assignedDate}</CardText>
                                        <CardText>{homework.dueDate}</CardText>
                                        <CardText>{homework.studentName}</CardText>
                                     </Grid>
        
                                    <Grid item xs={4} style={{backgroundColor: "#E4E7EB", marginBottom:"3vw" }}>
                                        <Container>
                                        <Button className={classes.btnUpdate}>
                                        <Link className={classes.btnUpdate} to={{pathname:"/updatehomework"}}>
                                        Edit
                                        </Link>
                                        </Button>
                                        <Button className={classes.btnDelete} onClick={() => {
                                            deleteHomeworkData(values.homeWorkId).then((response)=>{
                                                setData(response.data)});}}>
                                        Delete
                                        </Button>
                                        </Container>     
                                    </Grid>
                                    </Grid>
                                    </div>    
                                )
                            } 
                        </CardContent>             
    </div>
);

};

export default Homework;