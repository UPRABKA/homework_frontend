import React from "react";
import { Link } from "react-router-dom";
import { ListItemText,Collapse } from "@material-ui/core";
import { List, ListItem,ListItemIcon } from "@material-ui/core";
import DashboardIcon from "@material-ui/icons/Dashboard";
import DescriptionIcon  from "@material-ui/icons/Description";
import { ExpandLess, ExpandMore } from "@material-ui/icons";


const Listitem=()=>{

    const [insOpen, setInsOpen] = React.useState(false)
    const handleInstructorClick = () => {
        setInsOpen(!insOpen);
    }

    const [stuOpen, setStuOpen] = React.useState(false)
    const handleStudentClick = () => {
        setStuOpen(!stuOpen);
    }

    const logOut=()=>{

        localStorage.clear();
       // props.history.push('/');

    }


return (
    <div>
    <ListItem button onClick={handleInstructorClick} >
        <ListItemIcon>
             <DescriptionIcon/>
        </ListItemIcon>
        <ListItemText primary="Instructor"/>
        {insOpen ? <ExpandLess/> : <ExpandMore/>}
    </ListItem>
    <Collapse
    in={insOpen}
    timeout="auto"
    unmountOnExit>
    <List component="div" disablePadding>
    <ListItem button component={Link} to="/allhomeworks">
        <ListItemIcon>
            <DashboardIcon/>
        </ListItemIcon>
        <ListItemText primary="Manage Homeworks"/>
    </ListItem>
    </List>
    <List component="div" disablePadding>
    <ListItem button component={Link} to="/addhomework">
        <ListItemIcon>
            <DashboardIcon/>
        </ListItemIcon>
        <ListItemText primary="Add HomeWork"/>
    </ListItem>
    </List>
    <List component="div" disablePadding>
    <ListItem button component={Link} to="/updatehomework">
        <ListItemIcon>
            <DashboardIcon/>
        </ListItemIcon>
        <ListItemText primary="Update HomeWork"/>
    </ListItem>
    </List>
    </Collapse>

    <ListItem button onClick={handleStudentClick} >
        <ListItemIcon>
             <DescriptionIcon/>
        </ListItemIcon>
        <ListItemText primary="Student"/>
        {stuOpen ? <ExpandLess/> : <ExpandMore/>}
    </ListItem>

    <Collapse
    in={stuOpen}
    timeout="auto"
    unmountOnExit>
    <List component="div" disablePadding>
    <ListItem button component={Link} to="/viewhomeworks">
        <ListItemIcon>
            <DashboardIcon/>
        </ListItemIcon>
        <ListItemText primary="View Homeworks"/>
    </ListItem>
    </List>
    </Collapse>
    
    <ListItem button component={Link} to="/users">
        <ListItemIcon>
            <DashboardIcon/>
        </ListItemIcon>
        <ListItemText primary="All Users"/>
    </ListItem>
    <ListItem button component={Link} to="/"
     onClick={() =>logOut()}>
        <ListItemIcon>
            <DashboardIcon/>
        </ListItemIcon>
        <ListItemText primary="Log Out"/>
    </ListItem>

</div>
);
};
export default Listitem;