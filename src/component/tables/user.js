import React, { useState } from "react";
import {makeStyles,withStyles} from "@material-ui/core/styles";
import {Table,TableBody,TableCell,TableHead,TableRow} from "@material-ui/core";
import {fetchUsersData} from "../../api/authenticationService";

const useStyles = makeStyles((theme) => ({
table: {
    padding: "1vw",
    paddingTop: "0px",
    borderSpacing: "0 5px",
    borderCollapse: "separate",
},
headText: {
    backgroundColor: "#424242",
    width: "10vw",
    color: "white",
    fontFamily: "rubik",
    fontSize: "0.9vw",
    textAlign: "center",
    borderRadius: "1vw",
    padding: "0.1vw",
    [theme.breakpoints.down('sm')]: {
        fontSize: "1.6vw",
        width: "8.5vw",
        borderRadius: "2vw",
    },
    [theme.breakpoints.down('xs')]: {
        fontSize: "2vw",
        width: "8vw",
        borderRadius: "4vw",
    },
},

}))
const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "white",
        color: "black",
        fontSize: "4vw",
        textAlign: "-webkit-center",
    },
    body: {
        fontSize: "1vw",
    }
}))(TableCell);
const StyledTableRow = withStyles((theme) => ({
    root: {
        height: "3vw",
        backgroundColor: "#E4E7EB",
    }
}))(TableRow);

const Users =(props) => {
const classes = useStyles();

const [data, setData]=useState([])
  
React.useEffect(()=>{
    fetchUsersData().then((response)=>{
        setData(response.data);
    }).catch((e)=>{
        localStorage.clear();
        props.history.push('/');
    })
},[])

return(
    <div className={classes.root}>
        <Table className={classes.table} aria-label="custom pagination table" stickyHeader>
        <TableHead>
        <TableRow>
        <StyledTableCell style={{width:'15vw'}}>
            <div className={classes.headText}>Name</div>
        </StyledTableCell>
        <StyledTableCell style={{width:'15vw'}}>
            <div className={classes.headText}>Username</div>
        </StyledTableCell>
        <StyledTableCell style={{width:'15vw'}}>
            <div className={classes.headText}>Role</div>
        </StyledTableCell> 
        </TableRow>
        </TableHead>
        <TableBody>
        {
        data.map(users =>
        <StyledTableRow>
            <StyledTableCell align='center'>{users.name}</StyledTableCell>
            <StyledTableCell align='center'>{users.username}</StyledTableCell>
            <StyledTableCell align='center'>{users.roles}</StyledTableCell>
        </StyledTableRow>                                            
        )
        } 
        </TableBody>
        </Table>
    </div>
);
};
export default Users;