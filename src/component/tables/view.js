import React, { useState } from "react";
import {makeStyles,withStyles} from "@material-ui/core/styles";
import {Table,TableBody,TableCell,TableHead,TableRow} from "@material-ui/core";
import {fetchHomeworkData} from "../../api/authenticationService";

const useStyles = makeStyles((theme) => ({
table: {
    padding: "1vw",
    paddingTop: "0px",
    borderSpacing: "0 5px",
    borderCollapse: "separate",

},
headText: {
    backgroundColor: "#424242",
    width: "10vw",
    color: "white",
    fontFamily: "rubik",
    fontSize: "0.9vw",
    textAlign: "center",
    borderRadius: "1vw",
    padding: "0.1vw",
    [theme.breakpoints.down('sm')]: {
        fontSize: "1.6vw",
        width: "8.5vw",
        borderRadius: "2vw",
    },
    [theme.breakpoints.down('xs')]: {
        fontSize: "2vw",
        width: "8vw",
        borderRadius: "4vw",
    },
},
}))
const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "white",
        color: "black",
        fontSize: "4vw",
        textAlign: "-webkit-center",
    },
    body: {
        fontSize: "1vw",
    }
}))(TableCell);
const StyledTableRow = withStyles((theme) => ({
    root: {
        height: "3vw",
        backgroundColor: "#E4E7EB",
    }
}))(TableRow);


const View =(props) => {
    const classes = useStyles();
    
    const [data, setData]=useState([])
      
    React.useEffect(()=>{
        fetchHomeworkData().then((response)=>{
            setData(response.data);
        }).catch((e)=>{
            localStorage.clear();
            props.history.push('/');
        })
    },[])

return(
    <div className={classes.root}>
            <Table className={classes.table} aria-label="custom pagination table" stickyHeader>
                                <TableHead>
                                    <TableRow>
                                        <StyledTableCell style={{width:'15vw'}}>
                                            <div className={classes.headText}>Title</div>
                                        </StyledTableCell>
                                        <StyledTableCell style={{width:'15vw'}}>
                                            <div className={classes.headText}>Instructor Name</div>
                                        </StyledTableCell>
                                        <StyledTableCell style={{width:'15vw'}}>
                                            <div className={classes.headText}>Course</div>
                                        </StyledTableCell>
                                        <StyledTableCell style={{width:'15vw'}}>
                                            <div className={classes.headText}>Description</div>
                                        </StyledTableCell>
                                        <StyledTableCell style={{width:'15vw'}}>
                                            <div className={classes.headText}>Status</div>
                                        </StyledTableCell>
                                        <StyledTableCell style={{width:'15vw'}}>
                                            <div className={classes.headText}>Assign Date</div>
                                        </StyledTableCell>
                                        <StyledTableCell style={{width:'15vw'}}>
                                            <div className={classes.headText}>Due Date</div>
                                        </StyledTableCell>  
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                {
                                data.map(homework =>
                                        <StyledTableRow>
                                            <StyledTableCell align='center'>{homework.homeWorkTitle}</StyledTableCell>
                                            <StyledTableCell align='center'>{homework.instructorName}</StyledTableCell>
                                            <StyledTableCell align='center'>{homework.courseName}</StyledTableCell>
                                            <StyledTableCell align='center'>{homework.description}</StyledTableCell> 
                                            <StyledTableCell align='center'>{homework.status}</StyledTableCell> 
                                            <StyledTableCell align='center'>{homework.assignedDate}</StyledTableCell>
                                            <StyledTableCell align='center'>{homework.dueDate}</StyledTableCell>
                                        </StyledTableRow>                                            
                                 )
                                } 
                                </TableBody>
            </Table>
    </div>
);
};
export default View;